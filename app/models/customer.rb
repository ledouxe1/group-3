class Customer < ActiveRecord::Base
  attr_accessible :city, :email, :fname, :lname, :phone, :state, :street, :zip, :state_id

  has_many :orders
  has_many :bikes, :through => :orders
  belongs_to :state
end
