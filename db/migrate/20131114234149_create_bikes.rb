class CreateBikes < ActiveRecord::Migration
  def change
    create_table :bikes do |t|
      t.string :model
      t.float :price
      t.references :type
      t.timestamps
    end
  end
end
