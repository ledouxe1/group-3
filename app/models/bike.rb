class Bike < ActiveRecord::Base
  attr_accessible :model, :price, :type, :type_id, :desc

  belongs_to :type
  has_many :orders
  has_many :customers, :through => :orders

end
