class Order < ActiveRecord::Base
  attr_accessible :fname, :lname, :model, :price, :type, :bike, :customer, :bike_id, :customer_id

  belongs_to :bike
  belongs_to :customer
end
