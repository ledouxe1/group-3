class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.string :desc

      t.timestamps
    end
  end
end
