class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname
      t.string :lname
      t.string :street
      t.string :city
      t.references :state
      t.string :zip
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
