class Type < ActiveRecord::Base
  attr_accessible :desc

  has_many :bikes
end
